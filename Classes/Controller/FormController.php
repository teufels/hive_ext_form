<?php
namespace HIVE\HiveExtForm\Controller;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * FormController
 */
class FormController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * formRepository
     *
     * @var \HIVE\HiveExtForm\Domain\Repository\FormRepository
     * @inject
     */
    protected $formRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {

    }
}
