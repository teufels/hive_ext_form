<?php
namespace HIVE\HiveExtForm\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Forms
 */
class FormRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
